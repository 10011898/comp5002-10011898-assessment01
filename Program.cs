﻿using System;

namespace comp5002_10011898_assessment01
{
    class Program
    {
        public static void shopWelcome()
        {
                                                                                                        // Stored as a reference so that I don't have to clutter my code with this every time I want to draw the header
            Console.Clear();                                                                            // Used for clearing the console to keep the header at the top
            Console.ForegroundColor = ConsoleColor.Red;                                                 // Experimenting with colours to brighten the users experience
            Console.WriteLine("________-----________");                                                 // Fancy ascii art you can't learn in school
            Console.ForegroundColor = ConsoleColor.Yellow;                                              // Sets the main text as yellow. I was going to set it as white but it resembled the french flag too much so I decided to change it
            Console.WriteLine(" Welcome to my shop! ");                                                 // Give the user a friendly welcome to the shop
            Console.ForegroundColor = ConsoleColor.Blue;                                                // Setting the bottom ascii masterpiece to blue
            Console.WriteLine("--------_____--------");                                                 // Better than any art museum
            Console.ForegroundColor = ConsoleColor.White;                                               // Keeping the users text white past the header
            Console.WriteLine();                                                                        // The first of many line breaks to ensure an easy reading experience for the user
        }
        static void Main(string[] args)
        {
            
            string usrName = "";                                                                        // Stores the users chosen name as a string
            string itemChoice = "";                                                                     // Detemines if the user wants to add another item to the total price or not
            double numOne = 0;                                                                          // Stores the users price as a double for later use

            shopWelcome();                                                                              // Calling on the header reference

            Console.WriteLine("What is your name?");
            usrName = Console.ReadLine();                                                               // Saves the users response to the name as a string

            shopWelcome();                                                                              // Calling on the header reference
            Console.WriteLine($"Hi, {usrName}");
            Console.WriteLine("Please input a price to two decimal places.");                           // The user isnt REQUIRED to enter a number to two decimal places since everything will be rounded in the final total anyway
            numOne = Double.Parse(Console.ReadLine());                                                  // Stores the users number as a double variable

            shopWelcome();                                                                              // Calling on the header reference
            Console.WriteLine("Would you like to add another item? (y/n)");                             // Politely ask the user if they want to add another item
            itemChoice = Console.ReadLine();                                                            // Reads the users choice and stores it as a string variable

            if(itemChoice == "y")                                                                       // Runs this block of code if the user wants to add another item
            {
                shopWelcome();                                                                          // Calling on the header referece
                Console.WriteLine("Please enter the price of the second item.");                        // Ask the user to enter a second price
                numOne += double.Parse(Console.ReadLine());                                             // Converts the string to a double and adds it to the total
            }
                shopWelcome();                                                                          // Calling on the header reference
                Console.WriteLine($"Your total incl. GST is {numOne * 1.15:C2}.");                      // Calculates the final price, increasing the price by 15% to account for GST and displaying the value in $0.00 format
                Console.WriteLine();                                                                    // Tidy kiwi line break
                Console.ForegroundColor = ConsoleColor.Green;                                           // Adding some colour to keep things interesting for the user, green supports positive reinforcement
                Console.WriteLine($"Thank you for shopping with us today, {usrName}!");                 // Thank the user with a polite and fitting word for a shop
                Console.WriteLine("Press Any Key to exit the application.");                            // Nice way to end the application, makes the user feel in control
                Console.ReadKey();                                                                      // Allow the user to read over what has been entered and printed before pressing a key to exit.
        }
    }
}
